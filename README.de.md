# X-I2P-Location

## Zweck

Beim X-I2P-Location Header handelt es sich um einen HTTP Header, der dazu dient, dem Browser mitzuteilen, dass die Website auch im I2P-Netzwerk verfügbar ist. Das "I2P In Private Browsing" Add-on zeigt dann beispielsweise einen Hinweis an.
X-I2P-Location ist dabei das I2P Äquivalent zu Tor's [Onion-Location](https://community.torproject.org/onion-services/advanced/onion-location/) Header.

## Woraus besteht der Header?

Der Wert des Headers besteht dabei aus einer .i2p-Adresse. Beispielsweise `example.i2p`. Auch die Angabe einer base32-Adresse ist möglich.

### .i2p vs .b32.i2p

*Soll ich eine .i2p oder eine .b32.i2p verwenden?* Beide haben Vor- und Nachteile.
Eine .i2p-Adresse ist leicht zu merken, sodass der Besucher dann nächstes Mal direkt auf die Eepsite könnte, anstatt erst einmal zur Clearnet-Seite zu gehen, um die Adresse herauszufinden. Jedoch sind .i2p-Adressen anfälliger für Angriffe, wenn ein Angreifer das Adressbuch des Besuchers manipuliert hat oder der Besucher ein bösartiges Adressbuch hinzugefügt hat. Das ein solcher Angriff bei aufmerksamen Verhalten vorkommt, ist aber recht unwahrscheinlich.
Ein weiterer Nachteil von .i2p-Adressen ist, dass diese - wenn nicht bereits im Adressbuch vorhanden - erst durch einen Jump-Service aufgelöst werden müssen. Dies sorgt dafür, dass der Besucher einige Klicks mehr braucht, um zur Eepsite zu gelangen.
Der Nachteil von .b32.i2p Adressen wiederum ist es, dass diese extrem schwer, wenn nicht sogar unmöglich, zu merken sind. Dies würde dazu führen, dass der Besucher jedes mal zur Clearnet Website gehen müsste, um die .b32.i2p herauszufinden. Alternativ kann er sich die Adresse speichern - beispielsweise in einem Lesezeichen.
Was man benutzt, ist also eine Abwägung zwischen Sicherheit und Komfort.

## Wie hinzufüge ich diesen Header zu meiner Seite?

Es gibt zwei verschiedene Formen des X-I2P-Location Headers: `I2P-Location` und `X-I2P-Location`. Die Header sind dabei 
case-insensitive. Dies bedeutet, dass die Groß- und Kleinschreibung egal ist. Beide Header können gleichwertig benutzt werden.
In den folgenden Beispielen gehe ich davon aus, dass nur der `X-I2P-Location` gesetzt wird.

### Direkt in der Seite

HTML bietet mit den [Meta-Tags](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta#http-equiv) eine Möglichkeit an, HTTP Header direkt im HTML Dokument zu setzen:
```html
<meta http-equiv="X-I2P-Location" content="http://example.i2p/">
```
Dieser Tag sollte im Head-Tag platziert werden.

### Apache

Apache ist ein recht alter Webserver - welchen man auch anweisen kann, den [HTTP-Header direkt bei der Auslieferung der Datei zu setzen](https://httpd.apache.org/docs/current/mod/mod_headers.html).
```apache
Header set X-I2P-Location "http://example.i2p%{REQUEST_URI}s"
```
Dieser kann im VirtualHost-Block oder in einem Directory-Block platziert werden.

Mit `%{REQUEST_URI}s` wird erreicht, dass an den X-I2P-Location Header automatisch das entsprechende Unterverzeichnis sowie die Datei angehängt wird.

### Nginx

Nginx ist ein Webserver, welcher häufig als Reverse Proxy eingesetzt wird - [auch bei diesem kann man den X-I2P-Location Header setzen](https://nginx.org/en/docs/http/ngx_http_headers_module.html):
```nginx
add_header X-I2P-Location http://example.i2p$request_uri;
```
Diese Anweisung kann im Server- oder Location-Block platziert werden.

Mit `$request_uri` wird das Gleiche erreicht, was oben mit `%{REQUEST_URI}s` bei Apache erreicht wird.

### Caddy

Caddy ist ein Webserver, welcher HTTPS automatisch aktiviert hat und damit bei einfachen Konfigurationen sehr beliebt ist - [auch hier ist es möglich den X-I2P-Location Header zu setzen](https://caddyserver.com/docs/caddyfile/directives/header):
```
header X-I2P-Location  http://example.i2p{path}
```

Mit `{path}` wird das Gleiche erreicht, was oben mit `%{REQUEST_URI}s` bei Apache erreicht wird.

## Überprüfung

Um zu schauen, ob der Header wirklich bei der Auslieferung gesetzt wird, gibt es verschiedene Möglichkeiten.

Unter Linux kann man die Tools wget oder curl verwenden:
```sh
$ wget --server-response --spider example.com
```
```sh
$ curl --output /dev/null --verbose --silent example.com
```

## X-I2P-TorrentLocation

**TODO: Need more information, for example, from idk**
