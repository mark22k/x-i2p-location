# X-I2P-Location

## Purpose

The X-I2P Location header is an HTTP header that is used to tell the browser that the website is also available on the I2P network. The "I2P In Private Browsing" add-on then displays a hint, for example.
X-I2P-Location is the I2P equivalent of Tor's [Onion-Location](https://community.torproject.org/onion-services/advanced/onion-location/) header.

## What does the header consist of?

The value of the header consists of an .i2p address. For example, `example.i2p`. It is also possible to specify a base32 address.

### .i2p vs .b32.i2p

*Should I use an .i2p or a .b32.i2p?* Both have advantages and disadvantages.
An .i2p address is easy to remember, so the visitor could then go directly to the eepsite next time instead of going to the clearnet page first to find out the address. However, .i2p addresses are more vulnerable to attacks if an attacker has manipulated the visitor's address book or the visitor has added a malicious address book. However, that such an attack occurs with attentive behavior is quite unlikely.
Another disadvantage of .i2p addresses is that - if not already present in the address book - they must first be resolved by a jump service. This causes the visitor to need a few more clicks to get to the eepsite.
The disadvantage of .b32.i2p addresses, on the other hand, is that they are extremely difficult, if not impossible, to remember. This would result in the visitor having to go to the Clearnet website every time to find out the .b32.i2p. Alternatively, the visitor can save the address - for example, in a bookmark.
So what to use is a trade-off between security and comfort.

## How do I add this header to my page?

There are two different forms of the X-I2P-Location header: 'I2P-Location' and 'X-I2P-Location'. The headers are 
case-insensitive. This means that the case is not important. Both headers can be used equally.
In the following examples I assume that only the `X-I2P-Location` is set.

### Directly in the page

HTML provides a way to set HTTP headers directly in the HTML document using [meta tags](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta#http-equiv):
```html
<meta http-equiv="X-I2P-Location" content="http://example.i2p/">
```
This tag should be placed in the head tag.

### Apache

Apache is a rather old web server - which you can also instruct to set the [HTTP header directly when delivering the file](https://httpd.apache.org/docs/current/mod/mod_headers.html).
```apache
Header set X-I2P-Location "http://example.i2p%{REQUEST_URI}s"
```
This can be placed in the VirtualHost block or in a directory block.

With `%{REQUEST_URI}s` it is achieved that the corresponding subdirectory and file is automatically appended to the X-I2P-Location header.

### Nginx

Nginx is a web server which is often used as a reverse proxy - [ you can set the X-I2P location header for it as well](https://nginx.org/en/docs/http/ngx_http_headers_module.html):
```nginx
add_header X-I2P-Location http://example.i2p$request_uri;
```
This statement can be placed in the server or location block.

Using `$request_uri` achieves the same thing as `%{REQUEST_URI}s` does for Apache above.

### Caddy

Caddy is a web server which has HTTPS enabled automatically and is therefore very popular for simple configurations - [here it is also possible to set the X-I2P location header](https://caddyserver.com/docs/caddyfile/directives/header):
```
header X-I2P-Location  http://example.i2p{path}
```

Using `{path}` achieves the same thing as `%{REQUEST_URI}s` above for Apache.

## Testing

To look if the header is really set on serving, there are several ways.

On Linux you can use the tools wget or curl:
```sh
$ wget --server-response --spider example.com
```
```sh
$ curl --output /dev/null --verbose --silent example.com
```

## X-I2P-TorrentLocation

**TODO: Need more information, for example, from idk**
